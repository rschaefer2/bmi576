// (jEdit options) :folding=explicit:collapseFolds=1:
//{{{ Package, imports
package main;

import java.util.*;

//}}}
/**
* RnaSubstitutionMatrix contains the substitution matrix which scores for RNA conformer replacing others
*/

public class RnaSubstitutionMatrix {
  
  //{{{ Constants
  //}}}

  //{{{ Variables
  HashMap<String, ArrayList<String>> substitutionMatrix;
  ArrayList<String> allValidConformers;
  //}}}

  //{{{ Constructor
  // this needs to create a ~50 x ~50 matrix of substitution from each conformer to every
  // other conformer
  public RnaSubstitutionMatrix() {
    substitutionMatrix = new HashMap<String, ArrayList<String>>();
    ArrayList<String> cluster1a = new ArrayList<String>(Arrays.asList("1a"));
    ArrayList<String> cluster33p = new ArrayList<String>(Arrays.asList("1m", "1l", "&a", "7a", "3a", "9a", "1g", "7d", "3d", "5d"));
    ArrayList<String> cluster33t = new ArrayList<String>(Arrays.asList("1e", "1c", "1f", "5j", "5n"));
    ArrayList<String> cluster32p = new ArrayList<String>(Arrays.asList("1b", "1[", "3b", "1z", "5z", "7p", "5p"));
    ArrayList<String> cluster32t = new ArrayList<String>(Arrays.asList("1t", "5q"));
    ArrayList<String> cluster32m = new ArrayList<String>(Arrays.asList("1o", "7r", "5r"));
    ArrayList<String> cluster23p = new ArrayList<String>(Arrays.asList("2a", "4a", "0a", "#a", "4g", "6g", "8d", "4d", "6d", "2g"));
    ArrayList<String> cluster23t = new ArrayList<String>(Arrays.asList("2h", "4n", "0i", "6n", "6j"));
    ArrayList<String> cluster23m = new ArrayList<String>(Arrays.asList("0k"));
    ArrayList<String> cluster22p = new ArrayList<String>(Arrays.asList("2[","4b","0b","4p","6p", "2z"));
    ArrayList<String> cluster22t = new ArrayList<String>(Arrays.asList("4s", "2u"));
    ArrayList<String> cluster22m = new ArrayList<String>(Arrays.asList("2o"));
    ArrayList<String> clusterunknown = new ArrayList<String>(Arrays.asList("3g"));
    ArrayList<String> clusterBad = new ArrayList<String>(Arrays.asList("!!"));
    insertListIntoHashmap(cluster1a);
    insertListIntoHashmap(cluster33p);
    insertListIntoHashmap(cluster33t);
    insertListIntoHashmap(cluster32p);
    insertListIntoHashmap(cluster32t);
    insertListIntoHashmap(cluster32m);
    insertListIntoHashmap(cluster23p);
    insertListIntoHashmap(cluster23m);
    insertListIntoHashmap(cluster23t);
    insertListIntoHashmap(cluster22p);
    insertListIntoHashmap(cluster22t);
    insertListIntoHashmap(cluster22m);
    insertListIntoHashmap(clusterunknown);
    allValidConformers = new ArrayList<String>();
    allValidConformers.addAll(cluster1a);
    allValidConformers.addAll(cluster33p);
    allValidConformers.addAll(cluster33t);
    allValidConformers.addAll(cluster32p);
    allValidConformers.addAll(cluster32t);
    allValidConformers.addAll(cluster32m);
    allValidConformers.addAll(cluster23p);
    allValidConformers.addAll(cluster23t);
    allValidConformers.addAll(cluster23m);
    allValidConformers.addAll(cluster22p);
    allValidConformers.addAll(cluster22t);
    allValidConformers.addAll(cluster22m);
    allValidConformers.addAll(clusterunknown);
  }
  //}}}
  
  private void insertListIntoHashmap(ArrayList<String> cluster) {
    for (String conf : cluster) {
      substitutionMatrix.put(conf, cluster);
    }
  }
  
  public ArrayList<String> getAllValidConformers() {
	  return allValidConformers;
  }

  /**
  * @param   firstConf     The first query pair of RNA structure sequence
  * @param   secondConf    The second query pair of RNA structure sequence
  * @return                float score of substitution between the two 
  */
  public float getScore(String firstConf, String secondConf) {
    if (firstConf.equals("__") || secondConf.equals("__")) return -10000;
    if (firstConf.equals("!!") || secondConf.equals("!!")) return -1;
    if (firstConf.equals("1a") && secondConf.equals("1a")) return (float)0;
    if (firstConf.equals(secondConf)) return 2;
    if (substitutionMatrix.containsKey(firstConf)) {
      ArrayList<String> cluster = substitutionMatrix.get(firstConf);
      if (cluster.contains(secondConf)) return 1;
      else return -2;
    } else {
      System.err.println("Unknown structure string "+firstConf+" or "+secondConf+" given to RnaSubstitutionMatrix");
    }
    return -6;
  }
  
}
