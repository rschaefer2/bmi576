// (jEdit options) :folding=explicit:collapseFolds=1:
//{{{ Package, imports
package main;

import java.util.*;
import java.util.Map.Entry;

//}}}
/**
* Class for storing basic information about an RNA conformer.
*
*/
public class RnaConformer {
  
  //{{{ Constants
  //}}}

  //{{{ Variables
  /** A set containing all valid conformers (to be filled in by VBC) */
  /** The conformer this object represents */
  String  conformer;
  String nucl;
  //}}}

  //{{{ Constructor
  public RnaConformer(String conf, String nucl) {
	  conformer = conf;
	  this.nucl = nucl;
  }
  //}}}

  /**
  * @return     The two character string corresponding to the RNA conformer this object represents.
  */
  public String getConformer() {
    return conformer;
  }
  
  /**
  * @param      testConf      String to be tested
  * @param		valid		boolean whether testConf is a valid conformer or not
  * @return                   boolean saying whether the input is a valid conformer
  * 
  */
  public static boolean isValidConformer(String testConf) {
	  RnaSubstitutionMatrix maxtrix = new RnaSubstitutionMatrix();
	  boolean valid = false;
	  for(Entry<String, ArrayList<String>> entry : maxtrix.substitutionMatrix.entrySet()){
		  if (testConf.equals(entry.getValue())){
			  valid = true;
			  break;
		  } 
		}
    return valid;
  }
  
}
