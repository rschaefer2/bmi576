package main.preprocess;
import main.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class Preprocess {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		long startTime = System.currentTimeMillis();

		String parentDir = args[0];
		File testdir = new File(parentDir);
		if (!testdir.exists()) throw new FileNotFoundException(testdir.toString()+" does not seem to exist!");
		String target_dir = "suitestrings";
		File dir = new File(parentDir, target_dir);
		File[] files = dir.listFiles();
		//ArrayList<RnaSequenceString> all = new ArrayList<RnaSequenceString>();

		HashMap<String, ArrayList<RnaSequenceString>> map = new HashMap<String, ArrayList<RnaSequenceString>>();
		//Create HashMap of Words
		RnaSubstitutionMatrix rsm = new RnaSubstitutionMatrix();
		//ArrayList<String> comb = new ArrayList<String>(Arrays.asList("1m", "1l", "&a", "7a", "3a", "9a", "1g", "7d", "3d", "5d", "1a", "1e", "1c", "1f", "5j",
		//		"1b", "1[", "3b", "1z", "5z", "7p", "1t", "5q", "1o", "7r","2a", "4a", "0a", "#a", "4g", "6g", "8d", "4d", "6d", "2h", "4n", "0i", "6n", "6j",
		//		"2[","4b","0b","4p","6p", "4s", "2o", "!!") );
		ArrayList<String> comb = rsm.getAllValidConformers();
		comb.add("!!");
		for( int i = 0; i < comb.size(); i++) {
			for( int j = 0; j < comb.size(); j++) {
				String s = "" + comb.get(i) + comb.get(j);
				ArrayList<RnaSequenceString> a = new ArrayList<RnaSequenceString>();
				map.put(s, a);
			}
		}

		/* 
        ArrayList<String> cluster1a = new ArrayList<String>(Arrays.asList("1a"));
        ArrayList<String> cluster33p = new ArrayList<String>(Arrays.asList("1m", "1l", "&a", "7a", "3a", "9a", "1g", "7d", "3d", "5d"));
        ArrayList<String> cluster33t = new ArrayList<String>(Arrays.asList("1e", "1c", "1f", "5j"));
        ArrayList<String> cluster32p = new ArrayList<String>(Arrays.asList("1b", "1[", "3b", "1z", "5z", "7p"));
        ArrayList<String> cluster32t = new ArrayList<String>(Arrays.asList("1t", "5q"));
        ArrayList<String> cluster32m = new ArrayList<String>(Arrays.asList("1o", "7r"));
        ArrayList<String> cluster23p = new ArrayList<String>(Arrays.asList("2a", "4a", "0a", "#a", "4g", "6g", "8d", "4d", "6d"));
        ArrayList<String> cluster23t = new ArrayList<String>(Arrays.asList("2h", "4n", "0i", "6n", "6j"));
        ArrayList<String> cluster22p = new ArrayList<String>(Arrays.asList("2[","4b","0b","4p","6p"));
        ArrayList<String> cluster22t = new ArrayList<String>(Arrays.asList("4s"));
        ArrayList<String> cluster22m = new ArrayList<String>(Arrays.asList("2o"));
        ArrayList<String> clusterBad = new ArrayList<String>(Arrays.asList("!!"));
		 */
		
		System.out.println("Processing Files...");
		
		for (File f : files) { 
			if(f.isFile() && f.getName().endsWith("out") ) {
				BufferedReader inputStream = null;

				try {
					//System.out.println(f.getName());
					ArrayList<String> con = new ArrayList<String>();
					ArrayList<Character> nuc = new ArrayList<Character>();
					inputStream = new BufferedReader( new FileReader(f) );
					String line; //form (!!G)

					while ( (line = inputStream.readLine() ) != null  ) {
						//System.out.println(line);
						String conformation;
						char nucleotide;
						int pos = 0;
						line = line.trim();
						while(pos <= line.length()-1 ) {
							//parse characters
							conformation = line.substring(pos,pos + 2);
							nucleotide = line.charAt(pos+2);
							//add to ArrayList
							con.add(conformation.toLowerCase());
							nuc.add(Character.toLowerCase(nucleotide));
							pos = pos + 3;
						}
					}

					//create rna sequence object for each file
					RnaSequenceString r = new RnaSequenceString(f.getName().substring(0, 4), con, nuc);

					//Hash all twomers of r
					for(int i = 0; i < con.size() - 2; i++ ) {
						if( (con.get(i).compareTo("__") != 0) &&  (con.get(i+1).compareTo("__" ) != 0 ) ) {
							
							String s = "" + con.get(i) + con.get(i+1); 
							ArrayList<RnaSequenceString> a = new ArrayList<RnaSequenceString>();
							if(!map.containsKey(s)) {
								System.out.println("missing word " + s);
							} else {
								a = map.get(s);
								if( !a.contains(r) )
									a.add(r);
								map.put(s, a);
							}
						}
					}
					//add to arraylist of SequenceStrings
					//all.add(r);
				}
				catch (IndexOutOfBoundsException e ) {
					System.out.println("Ooops");
					e.printStackTrace();
				}
				finally {
					if (inputStream != null) {
						inputStream.close();
					}
				}
			}
		}
		System.out.println("Saving Database...");
		SaveDatabase(parentDir, "map", map);

		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		
		String time = String.format("%d min, %d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(duration),
			    TimeUnit.MILLISECONDS.toSeconds(duration) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
			);
		System.out.println("Saved");
		System.out.println("Time: " + time ); 
		System.out.println("Testing Loading Database...");
		
		LoadDatabase(parentDir, "map");
		
		System.out.println("DONE");
		
		
		System.exit(0);
	}

	public static void SaveDatabase(String parentDir, String name, Object obj) {
		try {
			File serDir = new File(parentDir, "suitestrings/ser/");
			serDir.mkdir();
			File file = new File (parentDir, "suitestrings/ser/"+name+".ser");
			ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream(file));
			oo.writeObject(obj);
			oo.close();
		} catch (IOException ioe){
			System.out.println("Oops");
			ioe.printStackTrace();
		}
	}


	public static void LoadDatabase(String parentDir, String name) {
		try {
			 ObjectInputStream oi = new ObjectInputStream(new FileInputStream(new File(parentDir, "suitestrings/ser/map.ser")));
	          Object DatabaseIn = oi.readObject();
	          HashMap<String, ArrayList<RnaSequenceString>> m = (HashMap<String, ArrayList<RnaSequenceString>>)DatabaseIn;
	          //database.setHashMap(m);
	          oi.close();
		} catch (Exception exc){
			System.out.println("EXCEPTION: "+exc.toString());
		}
	}
}
