// (jEdit options) :folding=explicit:collapseFolds=1:
//{{{ Package, imports

//package main;
package main;

import java.util.*;
//}}}
/**
* RnaDatabaseMatch contains the information for a hit-extended result
*/

public class RnaDatabaseMatch implements Comparable<RnaDatabaseMatch> {
  
  //{{{ Constants
  //}}}

  //{{{ Variables
  RnaSequenceString dbStructureSeq;
  String hitWord;
  String matchedQuerySeq;
  String matchedDbSeq;
  float score;
  int position;
  //}}}

  //{{{ Constructor
  public RnaDatabaseMatch(RnaSequenceString strucSeq, String querySeq, String dbSeq, float score, int pos, String hW) {
    this.dbStructureSeq = strucSeq;
    this.matchedQuerySeq = querySeq;
    this.matchedDbSeq = dbSeq;
    this.score = score;
    this.position = pos;
    this.hitWord = hW;
  }
  //}}}
  
  public RnaSequenceString getRnaSequenceString() {
    return dbStructureSeq;
  }
  
  public String getMatchedQuerySequence() {
    return matchedQuerySeq;
  }
  
  public String getMatchedDatabaseSequence() {
    return matchedDbSeq;
  }
  
  public float getScore() {
    return score;
  }
  
  public int getPosition() {
    return position;
  }
  
  public int compareTo(RnaDatabaseMatch rdbm) {
    float diff = (this.score - rdbm.getScore());
    int l = this.matchedQuerySeq.length() - rdbm.matchedQuerySeq.length();
    if (diff < 0) 
    	return -1;
    else if (diff > 0) 
    	return 1;
    else {
    	if ( l < 0 )
    		return -1;
    	else if( l > 0)
    		return 1;
    	else 
    		return 0;
    }
  }
  
  public boolean equals(Object obj) {
    if (obj instanceof RnaDatabaseMatch) {
      RnaDatabaseMatch rdbm = (RnaDatabaseMatch) obj;
      if ( (this.score == rdbm.getScore() ) && (this.dbStructureSeq.pdb.equals(rdbm.dbStructureSeq.pdb) ) 
    		  && (this.position == rdbm.position) ) return true;
    }
    return false;
  }
  
  public String toString() {
	  return "Query"+"\t"+this.matchedQuerySeq+"\t"+this.score+"\n"+
			  dbStructureSeq.getPdb() + "\t" + this.matchedDbSeq + "\t" + this.score + "\t" + this.position + "\t" + this.hitWord;
  }
  
}
