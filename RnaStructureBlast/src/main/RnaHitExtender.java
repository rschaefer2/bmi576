// (jEdit options) :folding=explicit:collapseFolds=1:
//{{{ Package, imports
package main;

import java.util.*;

//}}}
/**
* RnaHitExtender is for extending the hits of a match
*/

public class RnaHitExtender {
  
  //{{{ Constants
  //}}}

  //{{{ Variables
  RnaSubstitutionMatrix scorerMatrix;
  //}}}

  //{{{ Constructor
  public RnaHitExtender() {
    scorerMatrix = new RnaSubstitutionMatrix();
  }
  //}}}

  /**
  * @return            ArrayList containing results that has been extended 
  */
  public ArrayList<RnaDatabaseMatch> extendHit(String hitQsWord, String hitDbWord, RnaSequenceString querySequence, RnaSequenceString dbSequence) {
    //System.out.print(hitQsWord+":"+hitDbWord+" ");
	boolean ehz = dbSequence.pdb.toUpperCase().equals("1FCW");
	boolean stop = hitQsWord.equals("2a!!");
    //System.out.println(dbSequence.getPdb()+" ");
    //if (dbSequence.getPdb().equals("1JJ2")) System.out.println(hitQsWord + ":" + hitDbWord);
    String firstQsConf = hitQsWord.substring(0,2);
    String secondQsConf = hitQsWord.substring(2,4);
    String firstDbConf = hitDbWord.substring(0,2);
    String secondDbConf = hitDbWord.substring(2,4);
    float maxScore;
    float score;
    ArrayList<String> structSeqList = dbSequence.getStructureSequence();
    ArrayList<String> querySeqList = querySequence.getStructureSequence();
    int[] queryIndexes = allIndexOfWord(hitQsWord, querySequence);
    int[] dbIndexes = allIndexOfWord(hitDbWord, dbSequence);
    ArrayList<RnaDatabaseMatch> results = new ArrayList<RnaDatabaseMatch>();
    int startIndex = -1;
    for (int dbj : dbIndexes) {
      score = scorerMatrix.getScore(firstQsConf, firstDbConf);
      score = score + scorerMatrix.getScore(secondQsConf, secondDbConf);
      maxScore = score;
      int j = dbj;
      String extendedDbHit = hitDbWord;
      String extendedQsHit = hitQsWord;
      for (int i = queryIndexes[0]-1; i >= 0; i--) { // starts at -1 since the hit is already in the score.
        if (j >= 1) {
          String dbConf = structSeqList.get(j-1);
          String qsConf = querySeqList.get(i);
          float potScore = scorerMatrix.getScore(qsConf, dbConf);
          if (potScore + score > 0) {
            score = potScore+score;
            if (score>maxScore) maxScore = score;
            extendedDbHit = dbConf + extendedDbHit;
            extendedQsHit = qsConf + extendedQsHit;
            startIndex = j-1;
          } else {
            break;
          }
          j--;
        } else {
          break;
        }
      }
      // reset the score for extending forwards??
      //score = scorerMatrix.getScore(firstQsConf, firstDbConf);
      //score = score + scorerMatrix.getScore(secondQsConf, secondDbConf);
      score = maxScore;
      j = dbj;
      for (int i = queryIndexes[0]+2; i < querySeqList.size(); i++) {
        if (j+2 < structSeqList.size()) {
          String dbConf = structSeqList.get(j+2);
          String qsConf = querySeqList.get(i);
          float potScore = scorerMatrix.getScore(qsConf, dbConf);
          if (potScore + score > 0) {
            score = potScore+score;
            if (score>maxScore) maxScore = score;
            extendedDbHit = extendedDbHit+dbConf;
            extendedQsHit = extendedQsHit+qsConf;
            if (startIndex == -1) startIndex = j; // in case going backwards stops immediately
          } else {
            break;
          }
          j++;
        } else {
          break;
        }
      }
      results.add(new RnaDatabaseMatch(dbSequence, extendedQsHit, extendedDbHit, maxScore, startIndex, hitDbWord));
    }
    return results;
  }
  
  public static int[] allIndexOfWord(String word, RnaSequenceString sequence){
    String firstConf = word.substring(0,2);
    String secondConf = word.substring(2,4);
    ArrayList<String> seqList = sequence.getStructureSequence();
    ArrayList<Integer> tempInts = new ArrayList<Integer>();
    for (int i = 0; i < seqList.size(); i++) {
      String testConf = seqList.get(i);
      if (firstConf.equals(testConf)) {
        if (i+1 < seqList.size()) {
          testConf = seqList.get(i+1);
          if (secondConf.equals(testConf)) {
            tempInts.add(new Integer(i));
          }
        }
      }
    }
    int[] indexes = new int[tempInts.size()];
    for (int i = 0; i < tempInts.size(); i++) {
      indexes[i] = tempInts.get(i).intValue();
    }
    return indexes;
  }
}
