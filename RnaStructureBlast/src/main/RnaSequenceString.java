// (jEdit options) :folding=explicit:collapseFolds=1:
//{{{ Package, imports

//package main;
package main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

//}}}
/**
 * RnaSequenceString represents a single RNA structure's string
 */

public class RnaSequenceString implements Serializable {

	//{{{ Constants
	/**
	 * Determines if a de-serialized file is compatible with this class.
	 *
	 * Maintainers must change this value if and only if the new version
	 * of this class is not compatible with old versions. See Sun docs
	 * for <a href=http://java.sun.com/products/jdk/1.1/docs/guide
	 * /serialization/spec/version.doc.html> details. </a>
	 *
	 * Not necessary to include in first version of the class, but
	 * included here as a reminder of its importance.
	 */
	private static final long serialVersionUID = 1L;
	//}}}

	/**
	 * 
	 */
	
	//{{{ Variables
	/**
	 *  PDB ID
	 * @serial
	 */
	String pdb;
	/**
	 * structure sequence
	 * @serial
	 */
	ArrayList<String> structureSequence;
	/**
	 * sequence of sugars
	 * @serial
	 */
	ArrayList<Character> nucleotideSequence;
	//}}}

	//{{{ Constructor
	public RnaSequenceString(String pdbId, ArrayList<String> structSeq, ArrayList<Character> nuclSeq) {
		pdb = pdbId;
		structureSequence = structSeq;
		nucleotideSequence = nuclSeq;
	}
	//}}}

	/**
	 * @return            PDB ID for this RnaSequenceString 
	 */
	public String getPdb() {
		return pdb;
	}

	/**
	 * @return            structure sequence for this RnaSequenceString 
	 */
	public ArrayList<String> getStructureSequence() {
		return structureSequence;
	}

	/**
	 * @return            nucleotide sequence for this RnaSequenceString 
	 */
	public ArrayList<Character> getNucleotideSequence() {
		return nucleotideSequence;
	}


	/**
	 * @return	true if file is the same?
	 */
	public boolean equals(Object obj) {
		if (obj instanceof RnaSequenceString) {
			RnaSequenceString rss = (RnaSequenceString) obj;
			if (rss.getPdb().equals(this.pdb) && 
				rss.getNucleotideSequence().equals(this.nucleotideSequence) &&
				rss.getStructureSequence().equals(this.structureSequence)) return true;
		}
		return false;
	}
	
	
	/**
	 * Writes Rnasequence to output file
	 * @param aOutputStream
	 * @throws IOException
	 */
	private void writeObject(ObjectOutputStream aOutputStream) throws IOException {
		//perform the default serialization for all non-transient, non-static fields
		aOutputStream.defaultWriteObject();
	}

	
	/**
	 * reads rnasequences in
	 * @param aInputStream
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
		//always perform the default de-serialization first
		aInputStream.defaultReadObject();

		//ensure that object state has not been corrupted or tampered with maliciously
		validateState();
	}

	private void validateState() {
		validateFileName(pdb);
		validateStructureSequence(structureSequence);
		validateNucleotideSequence(nucleotideSequence);
	}
	
	private void validateFileName(String str) {
		
	}
	
	private void validateStructureSequence(ArrayList<String> str) {
	}
	
	private void validateNucleotideSequence(ArrayList<Character> str) {
	}
}
