// (jEdit options) :folding=explicit:collapseFolds=1:
//{{{ Package, imports
package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.util.*;

//}}}
/**
* Main class for running the rest of the RNA structure blast code.
*/

public class RnaStructureBlastMain {
  
  //{{{ Constants
  //}}}

  //{{{ Variables
  //}}}

  // The main class to be called that runs all the code
  public static void main(String[] args) throws FileNotFoundException {
	  

	  int threshold = Integer.parseInt(args[1]);
	  
	  RnaDatabase database = new RnaDatabase();
	  
	  String parentDir = args[2];
	  File testdir = new File(parentDir);	
	  if (!testdir.exists()) throw new FileNotFoundException(testdir.toString()+" does not seem to exist!");
	  
	  long startTime = System.currentTimeMillis();
	  System.out.println("Loading Database...");
	  LoadDatabase(parentDir, database);
	  long endTime = System.currentTimeMillis();
	  System.out.println("Database took "+(endTime - startTime)/1000+" seconds to run");
	  
	  ///////////////////////////////////////////////////////
	  //Process input query into RnaSequenceString Object  //
	  ///////////////////////////////////////////////////////
	  System.out.println("Processing input query");
	  String query = args[0];
	  query = query.toLowerCase();
	  int pos = 0;
	  ArrayList<String> con = new ArrayList<String>();
	  ArrayList<Character> nuc = new ArrayList<Character>();
	  String conformation;
	  Character nucleotide;
	  while(pos <= query.length()-1 ) {
			//parse characters
			conformation = query.substring(pos,pos + 2);
			nucleotide = query.charAt(pos+2);
			//add to ArrayList
			con.add(conformation.toLowerCase());
			nuc.add(Character.toLowerCase(nucleotide));
			pos = pos + 3;
		}
	  RnaSequenceString q = new RnaSequenceString("query", con, nuc);
	  
	  ///////////////////////////////////////////
	  // Process input query for all two-mers  //
	  ///////////////////////////////////////////	  
	  ArrayList<String> words = conformerWords(con);
	  
	  
	  /////////////////////////////////////////////////
	  // Find hits of two-mers in database and score //
	  /////////////////////////////////////////////////
	  startTime = System.currentTimeMillis();
	  System.out.println("querying database");
	  ArrayList<RnaDatabaseMatch> matches = queryDatabase(words, q, database);
	  endTime = System.currentTimeMillis();
	  System.out.println("querying database took "+(endTime - startTime)/1000+" seconds to run");
	  
	  startTime = System.currentTimeMillis();
	  System.out.println("Processing results");
	  ArrayList<RnaDatabaseMatch> bestMatches = new ArrayList<RnaDatabaseMatch>();
	  for(int i = 0; i < matches.size(); i++) {
		  RnaDatabaseMatch matched = matches.get(i);
		  if(matched.getScore() >= threshold && !bestMatches.contains(matched) ) {
			  bestMatches.add(matched);
		  }
	  }
	  endTime = System.currentTimeMillis();
	  System.out.println("Processing results took "+(endTime - startTime)/1000+" seconds to run");
	  
	  Collections.sort(matches);
	  Collections.sort(bestMatches);
	  
	  if(bestMatches.isEmpty()) {
		  System.out.println("No matches scored above threshold. Top ten results: ");
		  for(int i = 10 ; i >= 1; i--)
			  System.out.println(matches.get(matches.size()-i).toString() );
	  }
	  for( RnaDatabaseMatch m : bestMatches ) {
		  System.out.println(m.toString() );
	  }

	  
	  System.out.print("Done");
	  System.exit(0);
	  
    
  }
  
  /**
   * @param	q	query String
   * @return 	ArrayList of Strings of two-mer words.  Excludes all "1a1a" words.
   */
  
  public static ArrayList<String> conformerWords(ArrayList<String> con) {
	  ArrayList<String> wordlist = new ArrayList<String>();
	
	  for(int i = 0; i < con.size()-1; i++) {
		  String twomer = "" + con.get(i) + con.get(i+1);
		  if (!twomer.equals("1a1a")) {
			  wordlist.add(twomer);
		  }
	  }
	  return wordlist;
  }

  /**
  * @param   querySequence  An ArrayList containing two-mer words ("1a1b1[2s")
  * @return                 String containing all matching RNA sequences from the database
  */
  public static ArrayList<RnaDatabaseMatch> queryDatabase(ArrayList<String> words, RnaSequenceString q, RnaDatabase db) {
	  
	  ArrayList<RnaDatabaseMatch> matches = new ArrayList<RnaDatabaseMatch>();
	  
	  RnaHitExtender extender = new RnaHitExtender();
	  for(String w : words) {
		  ArrayList<RnaSequenceString> hitlist = db.getSequenceStrings(w);
		  for(RnaSequenceString r : hitlist ) {
			ArrayList<RnaDatabaseMatch> m = extender.extendHit(w, w, q, r); 
			for(RnaDatabaseMatch t : m) {
				matches.add(t);
			}
		  }
	  }
	  return matches;
	  
    
  }
  
  public static void LoadDatabase(String parentDir, RnaDatabase db) {
	  try {
			 ObjectInputStream oi = new ObjectInputStream(new FileInputStream(new File(parentDir, "suitestrings/ser/map.ser")));
	          Object DatabaseIn = oi.readObject();
	          HashMap<String, ArrayList<RnaSequenceString>> m = (HashMap<String, ArrayList<RnaSequenceString>>)DatabaseIn;
	          db.setHashMap(m);
	          oi.close();
		} catch (Exception exc){
			System.out.println("EXCEPTION: "+exc.toString());
		}
  }
  
}
