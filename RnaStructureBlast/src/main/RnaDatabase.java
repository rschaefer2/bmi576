// (jEdit options) :folding=explicit:collapseFolds=1:
//{{{ Package, imports

//package main;
package main;

import java.util.*;

//}}}
/**
* RnaDatabase
*/

public class RnaDatabase {
  
  //{{{ Constants
  //}}}

  //{{{ Variables
  // hashmap of two RNA confomers strings(?) to an RnaSequenceString
  HashMap<String, ArrayList<RnaSequenceString>> confToSeqMap;
  //}}}

  //{{{ Constructor
  // This needs to take in all of the RNA sequence strings, parse out all possible pairs of conformer strings,
  // and store them in the HashMap
  public RnaDatabase() {
    confToSeqMap = null;
  }
  //}}}

  /**
  * @param   query     An input query pair of RNA structure sequence  ("1a1b")
  * @return            ArrayList containing all candidate RNA sequences for hit extending 
  */
  public ArrayList<RnaSequenceString> getSequenceStrings(String word) {
    return confToSeqMap.get(word);
  }
  
  public void setHashMap(HashMap<String, ArrayList<RnaSequenceString>> m) {
	  confToSeqMap = m;
  }
  
}
